﻿using UnityEngine;
using System.Collections;

namespace LastStandingSheep.Core
{
    public abstract class FSM<T> : MonoBehaviour
        where T : CharacterBase
    {
        protected StateBase<T> _state = null;

        protected virtual StateBase<T> ChangeState(StateBase<T> state)
        {
            _state = state;

            return _state;
        }

        protected void Update()
        {
            _state.Update();
        }
    }
}

