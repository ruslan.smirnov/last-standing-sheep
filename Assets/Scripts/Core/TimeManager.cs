﻿using UnityEngine;

namespace LastStandingSheep.Core
{
    public sealed class TimeManager : DataManager<TimeManager>
    {
        public bool IsPaused { get; private set; }
        public float CurrentDeltaTime { get { return IsPaused ? 0.0f : Time.deltaTime; } }

        public TimeManager()
        {

        }

        public void Setup()
        {

        }

        public void Pause()
        {
            IsPaused = true;
        }

        public void UnPause()
        {
            IsPaused = false;
        }
    }
}

