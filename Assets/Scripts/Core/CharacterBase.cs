﻿using UnityEngine;

namespace LastStandingSheep.Core
{
    public abstract class CharacterBase : MonoBehaviour
    {
        [SerializeField]
        protected CharacterController _controller = null;

        [SerializeField]
        protected float _speed = default;

        [SerializeField]
        protected float _weight = default;

        protected Vector3 _direction = Vector3.zero;
        protected bool _isMove = false;

        public bool IsMove { get { return _isMove; } }

        public virtual void Move(Vector3 position)
        {
            _direction = position;
            transform.LookAt(new Vector3(position.x, transform.position.y, position.z));
            _isMove = true;
        }

        protected virtual void Update()
        {
            var currentDeltaTime = TimeManager.Instance.CurrentDeltaTime;
            var offset = Vector3.zero;

            if (_isMove)
            {
                offset = _direction - transform.position;

                if (offset.magnitude > .5f)
                {
                    offset = offset.normalized * _speed;
                }
                else
                {
                    _isMove = false;
                }
            }

            offset += Physics.gravity * _weight;
            _controller.Move(offset * currentDeltaTime);
        }
    }
}

