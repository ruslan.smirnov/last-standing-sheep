﻿using UnityEngine;
using System.Collections;

namespace LastStandingSheep.Core
{
    public abstract class StateBase<T> where T : CharacterBase
    {
        public T Character { get; }

        public StateBase(T character)
        {
            Character = character;
        }

        public virtual void Update()
        {

        }
    }
}