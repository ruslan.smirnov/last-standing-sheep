﻿using System.Collections;

using UnityEngine;

namespace LastStandingSheep.Core
{
    public abstract class DataManager
    {

    }

    public class DataManager<T> : DataManager where T: DataManager<T>
    {
        public static T Instance { get; private set; }

        public DataManager() : base()
        {
            if (Instance != null)
            {
                throw new UnityException(string.Format(
                    "DataManager{0} already created!",
                    typeof(T).Name));
            }
            Instance = this as T;
        }
    }
}

