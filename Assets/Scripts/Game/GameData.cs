﻿using System.Collections.Generic;

using UnityEngine;

using LastStandingSheep.Game.Characters;

namespace LastStandingSheep.Game
{
    [System.Serializable]
    public sealed class GameData
    {
        public const string LAYER_DEER = "Deer";
        public const string LAYER_PLAYER = "Player";

        [SerializeField]
        private Terrain _terrain = null;

        [SerializeField]
        private Platform _platform = null;

        [SerializeField]
        private Player _player = null;

        [SerializeField]
        private Wolf _wolf = null;

        [SerializeField]
        private Ufo _ufo = null;

        [SerializeField]
        private List<Deer> _deers = new List<Deer>();

        [SerializeField]
        private float _timeDelayCollectDeers = default;

        [SerializeField]
        private float _timeStartCollectDeers = default;

        [SerializeField]
        private float _timePlatformWait = default;

        [SerializeField]
        private float _timeNexRaise = default;

        public Terrain Terrain { get { return _terrain; } }
        public Platform Platform { get { return _platform; } }
        public Player Player { get { return _player; } }
        public Wolf Wolf { get { return _wolf; } }
        public Ufo Ufo { get { return _ufo; } }
        public float TimeDelayCollectDeers { get { return _timeDelayCollectDeers; } }
        public float TimeStartCollectDeers { get { return _timeStartCollectDeers; } }
        public float TimePlatformWait { get { return _timePlatformWait; } }
        public float TimeNexRaise { get { return _timeNexRaise; } }
        public bool IsLockPlayerControl { get; private set; }

        public List<Deer> GetDeersAlive()
        {
            return _deers.FindAll(x => x.FSM.CurrentState != DeerFSM.States.Dead);
        }

        public List<Deer> GetDeersAliveAndPlatform()
        {
            return _deers.FindAll(x => x.FSM.CurrentState != DeerFSM.States.Dead && !x.IsOnPlatform());
        }

        public int GetDeersCountAlive()
        {
            var list = GetDeersAlive();
            return list == null ? 0 : list.Count;
        }

        public bool IsPlayerDead()
        {
            return Player.FSM.CurrentState == PlayerFSM.States.Dead;
        }

        public Vector3 GetTerrainRandomPosition()
        {
            Vector3 terrainSize = Terrain.terrainData.size;

            float x = Random.Range(2, terrainSize.x - 2);
            float z = Random.Range(2, terrainSize.z - 2);

            return new Vector3(x, 0, z);
        }

        public bool IsOnPlatform(Vector3 position)
        {
            Vector3 center = Platform.transform.position;
            Vector3 size = Platform.transform.localScale;

            float minX = center.x - size.x / 2;
            float minZ = center.z - size.z / 2;

            float maxX = center.x + size.x / 2;
            float maxZ = center.z + size.z / 2;

            var rect = new Rect(minX, minZ, size.x, size.z);

            return rect.Contains(new Vector2(position.x, position.z));
        }

        public Vector3 GetPlatformRandomPosition()
        {
            Vector3 center = Platform.transform.position;
            Vector3 size = Platform.transform.localScale;

            float minX = center.x - size.x / 2;
            float minZ = center.z - size.z / 2;

            float maxX = center.x + size.x / 2;
            float maxZ = center.z + size.z / 2;

            float x = Random.Range(minX, maxX);
            float z = Random.Range(minZ, maxZ);

            return new Vector3(x, 0, z);
        }

        public void SetLockControl(bool value)
        {
            IsLockPlayerControl = value;
        }
    }
}

