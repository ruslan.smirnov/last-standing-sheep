﻿using System.Collections.Generic;

using UnityEngine;
using UnityEngine.EventSystems;

using LastStandingSheep.Core;
using LastStandingSheep.Game.Characters;

namespace LastStandingSheep.Game
{
    public sealed class GameController : MonoBehaviour
    {
        public static GameController Instance;

        public GameData GameData { get { return _gameData; } }

        [SerializeField]
        private GameData _gameData = new GameData();

        [SerializeField]
        private GameViewer _gameViewer = null;

        private readonly Timer _timer = new Timer(0);

        public void RestartGame()
        {
            EntryPoint.Instance.RestartGame();
        }

        public void RunPlatformGame()
        {
            _gameData.Ufo.AbductionComplete += OnUfoAbductionCompleted;
            _gameData.Ufo.RunAbduction();
        }

        private void TryChangeStateDeersToEscape()
        {
            var deers = _gameData.GetDeersAlive();

            if (deers == null)
            {
                return;
            }

            foreach (Deer deer in deers)
            {
                if (deer.FSM.CurrentState != DeerFSM.States.Dead)
                {
                    deer.FSM.ChangeState(DeerFSM.States.Escape);
                }
            }
        }

        private void TryChangeStateDeersToDead()
        {
            var deers = _gameData.GetDeersAlive();

            if (deers == null)
            {
                return;
            }

            foreach (Deer deer in deers)
            {
                if (deer.FSM.CurrentState != DeerFSM.States.Dead
                    && !deer.IsOnPlatform())
                {
                    deer.FSM.ChangeState(DeerFSM.States.Dead);
                }
                else
                {
                    deer.FSM.ChangeState(DeerFSM.States.PlatformIdle);
                }
            }
        }

        private void TryChangeStateDeersToRunAway()
        {
            var deers = _gameData.GetDeersAlive();

            if (deers == null)
            {
                return;
            }

            foreach (Deer deer in deers)
            {
                if (deer.FSM.CurrentState != DeerFSM.States.Dead)
                {
                    deer.FSM.ChangeState(DeerFSM.States.RunAway);
                }
            }
        }

        private void TryKillPlayer()
        {
            if (!_gameData.Player.IsOnPlatform())
            {
                _gameData.Player.FSM.ChangeState(PlayerFSM.States.Dead);
            }
        }

        private void TryWolfHunt()
        {
            var victims = new List<CharacterBase>();
            var deers = _gameData.GetDeersAliveAndPlatform();

            if (deers != null)
            {
                victims.AddRange(deers);
            }

            if (!_gameData.Player.IsOnPlatform())
            {
                victims.Add(_gameData.Player);
            }

            _gameData.Wolf.RunHunt(victims);
        }

        private bool CheckGameOver()
        {
            var deersCount = _gameData.GetDeersCountAlive();
            var isPlayerDead = _gameData.IsPlayerDead();

            if (deersCount > 0 && !isPlayerDead)
            {
                return false;
            }
            else if (deersCount == 0 && !isPlayerDead)
            {
                _gameViewer.ShowVictory();
            }
            else
            {
                _gameViewer.ShowDefeat();
            }

            return true;
        }

        private void OnUfoAbductionCompleted()
        {
            _gameData.Ufo.AbductionComplete -= OnUfoAbductionCompleted;

            if (!CheckGameOver())
            {
                _gameData.Platform.ToHighlightShow();
                _timer.Complete += DelayToCollectDeersToPlatform;
                _timer.Reset(_gameData.TimeDelayCollectDeers);
                _timer.Run();
            }
        }

        private void DelayToCollectDeersToPlatform()
        {
            _timer.Complete -= DelayToCollectDeersToPlatform;

            TryChangeStateDeersToEscape();

            _timer.Complete += ToCollectDeersToPlatform;
            _timer.Reset(_gameData.TimeStartCollectDeers);
            _timer.Run();
        }

        private void ToCollectDeersToPlatform()
        {
            _gameData.SetLockControl(true);

            _timer.Complete -= ToCollectDeersToPlatform;

            _gameData.Platform.ToHighlightHide();

            _gameData.Platform.Raised += OnPlatformRaised;
            _gameData.Platform.ToUpper();
        }

        private void OnPlatformRaised()
        {
            _gameData.Platform.Raised -= OnPlatformRaised;

            //TO DO: если нужно без волка
            //_gameData.TryChangeStateDeersToDead();
            //_gameData.TryKillPlayer();

            //_timer.Complete += OnPlatformWaitCompleted;
            //_timer.Reset(_gameData.TimePlatformWait);
            //_timer.Run();

            _gameData.Wolf.HuntComplete += OnWolfHuntCompleted;
            TryWolfHunt();
        }

        private void OnWolfHuntCompleted()
        {
            _gameData.Wolf.HuntComplete -= OnWolfHuntCompleted;

            OnPlatformWaitCompleted();
        }

        private void OnPlatformWaitCompleted()
        {
            _gameData.SetLockControl(false);

            _timer.Complete -= OnPlatformWaitCompleted;
            _gameData.Platform.ToLowerComplete += OnPlatformToLowerCompleted;
            _gameData.Platform.ToLower();
        }

        private void OnPlatformToLowerCompleted()
        {
            _gameData.Platform.ToLowerComplete -= OnPlatformToLowerCompleted;

            var deersCount = _gameData.GetDeersCountAlive();
            var isPlayerDead = _gameData.IsPlayerDead();

            if (!CheckGameOver())
            {
                TryChangeStateDeersToRunAway();

                _timer.Complete += OnTimeNexRaiseCompleted;
                _timer.Reset(_gameData.TimeNexRaise);
                _timer.Run();
            }
        }

        private void OnTimeNexRaiseCompleted()
        {
            _timer.Complete -= OnTimeNexRaiseCompleted;
            RunPlatformGame();
        }

        public void ReceiveTouch(PointerEventData eventData)
        {
            if (!_gameData.IsLockPlayerControl)
            {
                Vector3 worldPoint = Camera.main.ScreenToWorldPoint(eventData.position);

                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(eventData.position);

                if (Physics.Raycast(ray, out hit, 100, LayerMask.GetMask("Ground")))
                {
                    _gameData.Player.Move(hit.point);
                }
            }
        }

        private void Awake()
        {
            Instance = this;
        }

        private void Update()
        {
            _timer.Update();

            if (Input.GetKeyDown(KeyCode.P))
            {
                TimeManager.Instance.Pause();
            }

            if (Input.GetKeyDown(KeyCode.U))
            {
                TimeManager.Instance.UnPause();
            }
        }
    }
}

