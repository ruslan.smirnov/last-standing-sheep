﻿using System;
using System.Collections.Generic;

using UnityEngine;

using DG.Tweening;

using LastStandingSheep.Core;

namespace LastStandingSheep.Game
{
    public sealed class Platform : MonoBehaviour
    {
        // TO DO: захардкорил для сохранения времени
        private readonly List<Vector2> _platformPositions = new List<Vector2>()
        {
            { new Vector2(16, 9) },
            { new Vector2(16, 16) },
            { new Vector2(16, 23) },
            { new Vector2(9, 9) },
            { new Vector2(9, 23) },
            { new Vector2(9, 16) },
            { new Vector2(23, 9) },
            { new Vector2(23, 16) },
            { new Vector2(23, 23) },
        };

        [SerializeField]
        private int _heightMax = default;

        [SerializeField]
        private int _heightMin = default;

        [SerializeField]
        private float _timePush = default;

        [SerializeField]
        private float _divider = default;

        [SerializeField]
        private GameObject _highlight = null;

        private readonly Timer _timer = new Timer(0);

        public Action Raised;
        public Action ToLowerComplete;

        public void ToHighlightShow()
        {
            int index = UnityEngine.Random.Range(0, _platformPositions.Count);
            transform.position = new Vector3(
                _platformPositions[index].x,
                _heightMin,
                _platformPositions[index].y);

            _highlight.SetActive(true);
        }

        public void ToHighlightHide()
        {
            _highlight.SetActive(false);
        }

        public void ToUpper()
        {
            Sequence sequence = DOTween.Sequence();
            sequence.onComplete += OnToUpperCompleted;
            sequence.Append(transform.DOMoveY(_heightMax, _timePush));
        }

        private void OnToUpperCompleted()
        {
            Raised?.Invoke();
        }

        public void ToLower()
        {
            Sequence sequence = DOTween.Sequence();
            sequence.onComplete += Rescale;
            sequence.Append(transform.DOMoveY(_heightMin, _timePush));
        }

        private void Rescale()
        {
            Vector3 currentScale = transform.localScale;

            transform.localScale = new Vector3(
                currentScale.x / _divider, 
                currentScale.y, 
                currentScale.z / _divider);

            ToLowerComplete?.Invoke();
        }

        private void Awake()
        {
            _highlight.SetActive(false);
        }

        private void Update()
        {
            _timer.Update();
        }
    }
}

