﻿using UnityEngine;
using UnityEngine.SceneManagement;

using LastStandingSheep.Core;

namespace LastStandingSheep.Game
{
    public sealed class EntryPoint : MonoBehaviour
    {
        public static EntryPoint Instance;

        private TimeManager _timeManager;

        public void RestartGame()
        {
            SceneManager.sceneUnloaded += OnSceneGameUnloaded;
            SceneManager.UnloadSceneAsync("Game", UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);
        }

        private void OnSceneGameUnloaded(Scene arg0)
        {
            SceneManager.sceneUnloaded -= OnSceneGameUnloaded;
            StartGame();
        }

        private void StartGame()
        {
            SceneManager.sceneLoaded += OnSceneGameLoaded;
            SceneManager.LoadSceneAsync("Game", LoadSceneMode.Additive);
        }

        private void OnSceneGameLoaded(Scene scene, LoadSceneMode loadMode)
        {
            SceneManager.SetActiveScene(scene);
            SceneManager.sceneLoaded -= OnSceneGameLoaded;
        }

        private void Awake()
        {
            Instance = this;

            if (_timeManager != null)
            {
                return;
            }

            _timeManager = new TimeManager();
        }

        private void Start()
        {
            StartGame();
        }
    }
}

