﻿using UnityEngine;
using UnityEngine.UI;

namespace LastStandingSheep.Game
{
    public sealed class GameViewer : MonoBehaviour
    {
        [SerializeField]
        private GameController _gameController = null;

        [SerializeField]
        private Button _startButton = null;

        [SerializeField]
        private Button _restartButton = null;

        [SerializeField]
        private GameObject _defeatBoard = null;

        [SerializeField]
        private GameObject _victoryBoard = null;

        public void ShowDefeat()
        {
            _defeatBoard.SetActive(true);
        }

        public void ShowVictory()
        {
            _victoryBoard.SetActive(true);
        }

        private void OnClickStrartButton()
        {
            _startButton.gameObject.SetActive(false);
            _gameController.RunPlatformGame();
        }

        private void OnClickRestrartButton()
        {
            _gameController.RestartGame();
        }

        private void Start()
        {
            _startButton.onClick.AddListener(OnClickStrartButton);
            _restartButton.onClick.AddListener(OnClickRestrartButton);
        }
    }
}

