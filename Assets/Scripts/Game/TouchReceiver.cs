﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace LastStandingSheep.Game
{
    public sealed class TouchReceiver : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        public void OnPointerDown(PointerEventData eventData)
        {
            //TO DO: OnPointerUp не работает без OnPointerDown
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            GameController.Instance.ReceiveTouch(eventData);
        }
    }
}

