﻿using UnityEngine;

using LastStandingSheep.Core;

namespace LastStandingSheep.Game.Characters
{
    public sealed class Deer : CharacterBase
    {
        [SerializeField]
        private DeerFSM _fsm = null;

        public DeerFSM FSM { get { return _fsm; } }

        public bool IsOnPlatform()
        {
            return GameController.Instance.GameData.IsOnPlatform(transform.position);
        }

        private void Awake()
        {
            FSM.Setup(this);
        }

        private void Start()
        {
            FSM.ChangeState(DeerFSM.States.Init);
        }
    }
}

