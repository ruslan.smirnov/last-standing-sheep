﻿using System;

using UnityEngine;

using LastStandingSheep.Core;

namespace LastStandingSheep.Game.Characters
{
    public sealed class Ufo : CharacterBase
    {
        public Action AbductionComplete;

        [SerializeField]
        private float _timeFlyToTarget = default;

        [SerializeField]
        private float _timeAbduction = default;

        [SerializeField]
        private float _timeFlyAway = default;

        [SerializeField]
        private UfoRay _ray = null;

        private readonly Timer _timer = new Timer(0);

        private Vector3 _startPosition;

        public void RunAbduction()
        {
            Vector3 targetPosition = GameController.Instance.GameData.GetTerrainRandomPosition();
            Move(new Vector3(targetPosition.x, _startPosition.y, targetPosition.z));

            _timer.Complete += OnFlyToTargerCompleted;
            _timer.Reset(_timeFlyToTarget);
            _timer.Run();
        }

        private void OnFlyToTargerCompleted()
        {
            _timer.Complete -= OnFlyToTargerCompleted;
            _ray.gameObject.SetActive(true);

            _timer.Complete += OnAbductionCompleted;
            _timer.Reset(_timeAbduction);
            _timer.Run();
        }

        private void OnAbductionCompleted()
        {
            _ray.gameObject.SetActive(false);
            _timer.Complete -= OnAbductionCompleted;

            Move(_startPosition);

            _timer.Complete += OnFlyAwayCompleted;
            _timer.Reset(_timeFlyAway);
            _timer.Run();
        }

        private void OnFlyAwayCompleted()
        {
            _timer.Complete -= OnFlyAwayCompleted;

            AbductionComplete?.Invoke();
        }

        private void TryKill(CharacterBase character)
        {
            if (character is Deer)
            {
                var deer = character as Deer;
                deer.FSM.ChangeState(DeerFSM.States.Dead);
            }
            else if (character is Player)
            {
                var player = character as Player;
                player.FSM.ChangeState(PlayerFSM.States.Dead);
            }
        }

        private void OnTriggerEntered(Collider other)
        {
            if (_ray.gameObject.activeSelf)
            {
                var target = other.gameObject.GetComponent<CharacterBase>();

                if (target != null)
                {
                    TryKill(target);
                }
            }
        }

        private void Awake()
        {
            _startPosition = transform.position;
            _ray.gameObject.SetActive(false);

            _ray.TriggerEnter += OnTriggerEntered;
        }

        protected override void Update()
        {
            base.Update();

            _timer.Update();
        }
    }
}

