﻿using System;
using System.Collections.Generic;

using UnityEngine;

using LastStandingSheep.Core;

namespace LastStandingSheep.Game.Characters
{
    public sealed class Wolf : CharacterBase
    {
        public Action HuntComplete;

        [SerializeField]
        private float _timeBetweenHunt = default;

        private readonly List<CharacterBase> _victims = new List<CharacterBase>();
        private int _currentVictimIndex = 0;
        private bool _isHunt = false;

        private Vector3 _startPosition;

        private readonly Timer _timer = new Timer(0);

        public void RunHunt(List<CharacterBase> deers)
        {
            _victims.Clear();
            _currentVictimIndex = 0;

            if (deers.Count <= 0)
            {
                HuntComplete?.Invoke();
                return;
            }

            _victims.AddRange(deers);

            transform.position = _startPosition;
            _isHunt = true;
            gameObject.SetActive(true);
        }

        private void OnTimeBetweenHuntOver()
        {
            _timer.Complete -= OnTimeBetweenHuntOver;

            var victim = _victims[_currentVictimIndex - 1];
            transform.position = victim.transform.position;
            TryKill(victim);
        }

        private void TryKill(CharacterBase character)
        {
            if (character is Deer)
            {
                var deer = character as Deer;
                deer.FSM.ChangeState(DeerFSM.States.Dead);
            }
            else if (character is Player)
            {
                var player = character as Player;
                player.FSM.ChangeState(PlayerFSM.States.Dead);
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            var target = other.transform.parent.GetComponent<CharacterBase>();

            if (target != null)
            {
                TryKill(target);
            }
        }

        private void Awake()
        {
            _startPosition = transform.position;
        }

        private void Start()
        {
            gameObject.SetActive(false);
        }

        protected override void Update()
        {
            base.Update();

            _timer.Update();

            if (_isHunt)
            {
                if (!IsMove && _currentVictimIndex < _victims.Count)
                {
                    var victim = _victims[_currentVictimIndex];

                    if (victim.gameObject.activeSelf)
                    {
                        Move(victim.transform.position);

                        _timer.Complete += OnTimeBetweenHuntOver;
                        _timer.Reset(_timeBetweenHunt);
                        _timer.Run();
                    }

                    _currentVictimIndex++;
                }
                else if (!IsMove && _currentVictimIndex == _victims.Count)
                {
                    _isHunt = false;
                    _victims.Clear();
                    _currentVictimIndex = 0;

                    HuntComplete?.Invoke();
                    gameObject.SetActive(false);
                }
            }
        }
    }
}

