﻿using UnityEngine;

using LastStandingSheep.Core;

namespace LastStandingSheep.Game.Characters
{
    public sealed class DeerFSM : FSM<Deer>
    {
        public enum States
        {
            Init,
            Idle,
            Escape,
            PlatformIdle,
            RunAway,
            Dead,
        }

        [SerializeField]
        private States _currentState;

        public States CurrentState { get; private set; }

        private Deer _character = null;

        public void Setup(Deer character)
        {
            _character = character;
            CurrentState = States.Idle;
        }

        public void ChangeState(States state)
        {
            if (CurrentState == States.Dead)
            {
                return;
            }

            switch (state)
            {
                case States.Init:
                    ChangeState(new DeerInit(_character));
                    CurrentState = States.Init;
                    break;
                case States.Idle:
                    ChangeState(new DeerIdle(_character));
                    CurrentState = States.Idle;
                    break;
                case States.Escape:
                    ChangeState(new DeerEscape(_character));
                    CurrentState = States.Escape;
                    break;
                case States.PlatformIdle:
                    ChangeState(new DeerPlatformIdle(_character));
                    CurrentState = States.PlatformIdle;
                    break;
                case States.RunAway:
                    ChangeState(new DeerRunAway(_character));
                    CurrentState = States.RunAway;
                    break;
                case States.Dead:
                    ChangeState(new DeerDead(_character));
                    CurrentState = States.Dead;
                    break;
                default:
                    break;
            }

            _currentState = CurrentState;
        }
    }
}

