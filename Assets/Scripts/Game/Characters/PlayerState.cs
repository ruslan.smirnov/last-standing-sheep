﻿using UnityEngine;

using LastStandingSheep.Core;

namespace LastStandingSheep.Game.Characters
{
    public abstract class PlayerState : StateBase<Player>
    {
        public PlayerState(Player character) : base(character)
        {

        }
    }

    public sealed class PlayerInit : PlayerState
    {
        public PlayerInit(Player character) : base(character)
        {
            Character.gameObject.transform.position = GameController.Instance.GameData.GetTerrainRandomPosition();
        }

        public override void Update()
        {
            base.Update();

            Character.FSM.ChangeState(PlayerFSM.States.Idle);
        }
    }

    public sealed class PlayerIdle : PlayerState
    {
        public PlayerIdle(Player character) : base(character)
        {
        }

        private void OnTriggerEntered(Collider other)
        {
        }
    }

    public sealed class PlayerDead : PlayerState
    {
        public PlayerDead(Player character) : base(character)
        {
            Character.gameObject.SetActive(false);
        }
    }
}