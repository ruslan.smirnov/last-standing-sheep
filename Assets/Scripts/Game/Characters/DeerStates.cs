﻿using UnityEngine;

using LastStandingSheep.Core;

namespace LastStandingSheep.Game.Characters
{
    public abstract class DeerState : StateBase<Deer>
    {
        public DeerState(Deer character) : base(character)
        {

        }
    }

    public sealed class DeerInit : DeerState
    {
        public DeerInit(Deer character) : base(character)
        {
            Character.gameObject.transform.position = GameController.Instance.GameData.GetTerrainRandomPosition();
        }

        public override void Update()
        {
            base.Update();

            Character.FSM.ChangeState(DeerFSM.States.Idle);
        }
    }

    public sealed class DeerIdle : DeerState
    {
        private const float MOVEMENT_COOLDOWN = 3.0f;

        private readonly Timer _timer = new Timer(0);

        public DeerIdle(Deer character) : base(character)
        {
        }

        public override void Update()
        {
            base.Update();

            _timer.Update();

            if (!Character.IsMove && !_timer.IsRunned)
            {
                Vector3 targetPosition = GameController.Instance.GameData.GetTerrainRandomPosition();
                Character.Move(targetPosition);

                _timer.Reset(Random.Range(0, MOVEMENT_COOLDOWN));
                _timer.Run();
            }
        }
    }

    public sealed class DeerEscape : DeerState
    {
        public DeerEscape(Deer character) : base(character)
        {
            Vector3 targetPosition = GameController.Instance.GameData.GetPlatformRandomPosition();
            Character.Move(targetPosition);
        }
    }

    public sealed class DeerPlatformIdle : DeerState
    {
        public DeerPlatformIdle(Deer character) : base(character)
        {
        }
    }

    public sealed class DeerRunAway : DeerState
    {
        public DeerRunAway(Deer character) : base(character)
        {
            Vector3 targetPosition = GameController.Instance.GameData.GetTerrainRandomPosition();

            Character.Move(targetPosition);
        }

        public override void Update()
        {
            base.Update();

            if (!Character.IsMove)
            {
                Character.FSM.ChangeState(DeerFSM.States.Idle);
            }
        }
    }

    public sealed class DeerDead : DeerState
    {
        public DeerDead(Deer character) : base(character)
        {
            Character.gameObject.SetActive(false);
        }
    }
}


