﻿using UnityEngine;

using LastStandingSheep.Core;

namespace LastStandingSheep.Game.Characters
{
    public sealed class Player : CharacterBase
    {
        [SerializeField]
        private PlayerFSM _fsm = null;

        public PlayerFSM FSM { get { return _fsm; } }

        public bool IsOnPlatform()
        {
            return GameController.Instance.GameData.IsOnPlatform(transform.position);
        }

        private void Awake()
        {
            FSM.Setup(this);
        }

        private void Start()
        {
            FSM.ChangeState(PlayerFSM.States.Init);
        }
    }

}

