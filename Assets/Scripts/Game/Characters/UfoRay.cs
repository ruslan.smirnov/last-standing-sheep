﻿using System;
using UnityEngine;

namespace LastStandingSheep.Game.Characters
{
    public sealed class UfoRay : MonoBehaviour
    {
        public Action<Collider> TriggerEnter;
        public Action<Collider> TriggerStay;

        private void OnTriggerEnter(Collider other)
        {
            TriggerEnter?.Invoke(other);
        }

        private void OnTriggerStay(Collider other)
        {
            TriggerStay?.Invoke(other);
        }
    }
}

