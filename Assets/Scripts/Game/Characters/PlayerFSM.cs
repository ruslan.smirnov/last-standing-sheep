﻿using UnityEngine;

using LastStandingSheep.Core;

namespace LastStandingSheep.Game.Characters
{
    public sealed class PlayerFSM : FSM<Player>
    {
        public enum States
        {
            Init,
            Idle,
            Dead,
        }

        [SerializeField]
        private States _currentState;

        public States CurrentState { get; private set; }

        private Player _character = null;

        public void Setup(Player character)
        {
            _character = character;
            CurrentState = States.Idle;
        }

        public void ChangeState(States state)
        {
            if (CurrentState == States.Dead)
            {
                return;
            }

            switch (state)
            {
                case States.Init:
                    ChangeState(new PlayerInit(_character));
                    CurrentState = States.Init;
                    break;
                case States.Idle:
                    ChangeState(new PlayerIdle(_character));
                    CurrentState = States.Idle;
                    break;
                case States.Dead:
                    ChangeState(new PlayerDead(_character));
                    CurrentState = States.Dead;
                    break;
                default:
                    break;
            }

            _currentState = CurrentState;
        }
    }
}